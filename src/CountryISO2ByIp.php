<?php

namespace GeoLocation;

/**
 * Class CountryByIp
 * @author Deepak Pandey <er.pandeydip@gmail.com>
 */
class CountryISO2ByIp
{

    const PATH_TO_IP_FILES = __DIR__ . '/../ips/';

    public function getISO2FromIP($ipAddress)
    {
        $ipSegments = explode('.', $ipAddress);
        $code = $this->generateCode($ipSegments);

        return $this->getCountryISO2($code, $ipSegments[0]);
    }

    /**
     * @param $firstDigitOnIp
     * @return array
     */
    private function requireFileBasedOnIp($firstDigitOnIp)
    {
        $ranges = require self::PATH_TO_IP_FILES . $firstDigitOnIp . '' . '.php';

        return $ranges;
    }

    /**
     * @param $ipSegments
     * @return integer
     */
    private function generateCode($ipSegments)
    {
        return ($ipSegments[0] * 16777216) + ($ipSegments[1] * 65536) + ($ipSegments[2] * 256) + ($ipSegments[3]);
    }

    /**
     * @param $code
     * @param $firstSegmentOfIp
     * @return string
     */
    private function getCountryISO2($code, $firstSegmentOfIp)
    {
        $ipCodeRanges = $this->requireFileBasedOnIp($firstSegmentOfIp);
        $countryISO2Code = '';
        foreach ($ipCodeRanges as $key => $value) {
            if ($key <= $code) {
                if ($ipCodeRanges[$key][0] >= $code) {
                    $countryISO2Code = $ipCodeRanges[$key][1];
                    break;
                }
            }
        }

        return $countryISO2Code;
    }

    /**
     * Check if request is from specific country
     * @param array $countryISO2Codes
     * @param string $ipAddress
     * @return bool
     */
    public function isRequestFromCountry(array $countryISO2Codes, $ipAddress)
    {
        $countryISO2Code = $this->getISO2FromIP($ipAddress);

        return array_key_exists($countryISO2Code, array_flip($countryISO2Codes));
    }
}
